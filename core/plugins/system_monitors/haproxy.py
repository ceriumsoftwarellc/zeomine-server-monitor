import subprocess
import json
from core.plugins.zsm_plugin import ZSMPlugin

## HAProxy Monitor
#
class HAProxy(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################

  # Define the parent ZSM object so we can call its functions and access its
  # data. Set this in load_config
  zsm_obj = None

  # Settings dict
  settings = {}
  settings['state'] = {}
  settings['state']['path'] = 'haproxy_state.json'
  # Path to HAProxy socket
  settings['stat_path'] = '/var/run/haproxy.sock'
  # Allow all servers (this will ignore the "servers" flag below.
  settings['allow_all_servers'] = False
  # Servers: dict with keys as family (pxname), value as list of server names 
  # (svname). you can also use a value of "all" to denote all servers as a family.
  settings['servers'] = {}

  # Alert settings
  #
  # Thresholds are a list organized by severity, as a dict with three values:
  # type: can be "change," "down," or "up" . Change means any change at all.
  # server_family: can be "all" for all families, or a string to denote a specific
  # family
  # amount: can be an integer or "all" to indicate all servers. Only used with a
  # type of "down" or "up."
  settings['alert'] = {}
  settings['alert']['thresholds'] = []

  # Test HAProxy alerting
  # Send a debug message with the current state of the server. Requires
  # a configured alert method.
  #
  # Takes a string in the form of the alert severity
  settings['alert_test'] = False

  # Report settings
  # Types
  #  - state: Saves historical data
  #  - TODO: downtime: Logs downtime in ZSM extended event format
  # Max
  #  - "Max" is a dict saying how many historical items to keep. Older items are deleted.
  settings['report'] = {}
  settings['report']['types'] = [] # may include 'downtime' or 'all'
  settings['report']['max'] = {'state': 10000, 'downtime': 10000}

  # Local data dict
  data = {}
  data['state'] = {}
  data['state']['current'] = {}
  data['state']['previous'] = {}
  data['state']['historical'] = []
  data['downtime'] = {}
  data['downtime']['current'] = {}
  data['downtime']['historical'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  ## Create an alert recommendation
  #
  # Standard thresholds are notice, warning, critical, and highly_critical.
  # You may create a custom severity to create extra user-specific cases.
  def create_alert(self, data, alert_type, threshold_value, severity = 'notice'):
    recommendation = {}
    recommendation['severity'] = severity
    recommendation['type'] = alert_type
    recommendation['threshold_value'] = threshold_value
    recommendation['data'] = data
    recommendation['reporter'] = self.__class__.__name__ # Usually the class name

    self.zsm_obj.data['alerts'].append(recommendation)

  def get_server_state(self):
    readstats = subprocess.check_output(["echo show stat | socat unix-connect:" + self.settings['stat_path'] + " stdio"], shell=True)
    stats = readstats.splitlines()
    ret_data = {}
    # Build data dict
    stat_data = {}
    stat_labels = str(stats[0]).replace("b'", "").replace("'","").replace('#', '').strip().split(',')
    stats.pop(0)
    for stat in stats:
      s = str(stat).replace("b'", "").replace("'","").replace('#', '').strip().split(',')
      if len(s) > 2:
        stat_data[s[0] + '-' + s[1]] = {}
        i = 0
        for k in stat_labels:
          if k:
            stat_data[s[0] + '-' + s[1]][k] = s[i]
          i += 1
    # Determine validity of stat. TODO update this method as its own function
    for stat in stat_data:
      if self.settings['allow_all_servers']:
        ret_data[stat] = stat_data[stat]
      # If the server is set to 
      elif stat_data[stat]['pxname'] in self.settings['servers']:
        if 'all' in self.settings['servers'][stat_data[stat]['pxname']] or stat_data[stat]['svname'] in self.settings['servers'][stat_data[stat]['pxname']]:
          ret_data[stat] = stat_data[stat]
      # If the server dict is set to the string "all," allow everything
      elif self.settings['servers'] == 'all' or 'all' in self.settings['servers']:
        ret_data[stat] = stat_data[stat]
    return ret_data

  def eval_states(self, old_state, new_state):
    change = False
    downcount = 0
    upcount = 0
    total = 0
    srv_stats = {'new':{}, 'old':{}}
    al = ''
    for srv in new_state:
      # If the server wasn't present last time, don't do the comparison.
      if srv in old_state and 'status' in old_state[srv] and 'status' in new_state[srv]:
        total += 1
        # Check if the server is up or down
        if new_state[srv]['status'] == 'UP':
          upcount += 1
        else:
          downcount += 1
        srv_stats['new'][srv] = new_state[srv]['status']
        srv_stats['old'][srv] = old_state[srv]['status']
        # Check if there is a status change
        if new_state[srv]['status'] != old_state[srv]['status']:
          change = True
        # Create standard alert data, just in case
        al += srv + ': ' + ' > '.join([old_state[srv]['status'], new_state[srv]['status']]) + '\n'
    # Log the standard alert data, and also print if pprint is set to high verbosity
    self.zsm_obj.pprint("HAProxy Status:", 2, 1)
    self.zsm_obj.pprint(al, 2, 1)
    # Evaluate alert conditions if a change has happened.
    if change:
      self.zsm_obj.pprint('change',0)
      for threshold in self.settings['alert']['thresholds']:
        if threshold['type'] == "change":
          val = threshold['type']
          self.create_alert(al, threshold['type'], val, threshold['severity'])
        elif 'amount' in threshold and \
        threshold['amount'] == "all" and \
        (threshold['type'] == 'up' and upcount == total) or \
        (threshold['type'] == 'down' and downcount == total):
          val = str(threshold['amount'])
          self.create_alert(al, threshold['type'], val, threshold['severity'])
        elif 'amount' in threshold and \
        (threshold['type'] == 'up' and upcount == threshold['amount']) or \
        (threshold['type'] == 'down' and downcount == threshold['amount']):
          val = str(threshold['amount'])
          self.create_alert(al, threshold['type'], val, threshold['severity'])


  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings:
              self.settings[section][subsection] = conf[section][subsection]
            else:
              self.settings[section] = {}
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          self.settings[section] = conf[section]

  ## Load Previous State for running comparisons
  #
  #
  def load_previous_state(self):
    path = self.zsm_obj.settings['user_data']['data'] + self.settings['state']['path']
    try:
      with open(path, 'r') as f:
        self.data = json.load(f)
    except:
      self.zsm_obj.pprint('Failed to import HAProxy state file at: ' + path,0)

  ## Collect Data
  #
  #
  def get_data(self):
    self.data['state']['current'] = self.get_server_state()

  ## Evaluate Data
  #
  #
  def evaluate_data(self):
    if self.data['state']['current']:
      self.create_report_data(self.data['state']['current'], 'zsm_haproxy') 
    if self.data['state']['current'] and self.data['state']['previous']:
      self.eval_states(self.data['state']['previous'], self.data['state']['current'])
    if self.settings['alert_test']:
      self.create_alert(str(self.data['state']['current']), 'haproxytest', '0', self.settings['alert_test'])

  ## Final actions for ZSM shutdown
  #
  # For his function, we will write the current HAProxy state
  def shutdown(self):
    # Save current state if we have recording turned on
    if 'state' in self.settings['report']['types']:
      add = {}
      for i in self.data['state']['current']:
        add[i] = self.data['state']['current']
      self.data['state']['historical'].append(add)
    # Save downtime info if enabled
    if 'downtime' in self.settings['report']['types']:
      if 'end' in self.data['downtime']['current'] and self.data['downtime']['current']['end']:
        add = {}
        for i in self.data['downtime']['current']:
          add[i] = self.data['downtime']['current']
        self.data['downtime']['historical'].append(add)
    # Clear out old records
    for typ in self.settings['report']['max']:
      if int(self.settings['report']['max'][typ]) and self.data[typ]['historical']:
        while len(self.data[typ]['historical']) > self.settings['report']['max'][typ] and self.data[typ]['historical']:
          self.data[typ]['historical'].pop(0)
    # Rotate the server state
    add = {}
    for i in self.data['state']['current']:
      add[i] = self.data['state']['current'][i]

    self.data['state']['previous'] = add
    # Write the state to file
    path = self.zsm_obj.settings['user_data']['data'] + self.settings['state']['path']
    with open(path,'w') as f:
      write_data = json.dumps(self.data)
      f.write(write_data)
