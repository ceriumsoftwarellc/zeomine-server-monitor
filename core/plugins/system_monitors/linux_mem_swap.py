import csv
from core.plugins.zsm_plugin import ZSMPlugin

## Linux System/Memory Load
#
# Records system load and memory usage. If certain thresholds are met, and alert
# threshold is met, and alert recommendation is sent.
#
class LinuxMemSwap(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  settings = {}
  
  ## Alert Thresholds
  #
  # Load averages will vary by system.
  # Setting should be a dict of severity:threshold values. 
  # For example, if a system load of 2.0 is the max:
  #
  # mem_usage: {'notice': 50.0, 'warning': 70.0, 'critical':90.0, 'highly_critical': 99.0}
  # Memory is calculated as a percentage of free memory from /proc/meminfo, and 
  # percentages should be given as a float, and a whole percentage, i.e. 20% = 20.0
  settings['alert_thresholds'] = {}
  settings['alert_thresholds']['mem_usage'] = False
  settings['alert_thresholds']['swap_usage'] = False
  
  # Save dat to main ZSM object at the end of collection?
  settings['save_data'] = False

  # Local Data
  data = {}
  data['collected'] = {}
  data['evaluated'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################
  def get_highest_threshold(self, val, threshold_dict):
    ret = False
    for t in threshold_dict:
      if val > float(threshold_dict[t]):
        ret = t
      else:
        pass
    return ret

  def eval_memory(self):
    load = self.data['collected']['memory'].split('\n')
    self.data['evaluated']['memory'] = {}
    mem_data = {}
    for line in load:
      l = line.split(':')
      if l[0] in ['MemTotal', 'MemFree', 'SwapTotal', 'SwapFree']:
        l[1] = l[1].replace('kB', '').strip()
        mem_data[l[0]] = float(l[1])
    self.data['evaluated']['data'] = mem_data
    if 'MemFree' in mem_data and 'MemTotal' in mem_data and mem_data['MemTotal']:
      self.data['evaluated']['mem_usage'] = 100.00 - (mem_data['MemFree']/mem_data['MemTotal']) * 100.00
    if 'SwapFree' in mem_data and 'SwapTotal' in mem_data and mem_data['SwapTotal']:
      self.data['evaluated']['swap_usage'] = 100.00 - (mem_data['SwapFree']/mem_data['SwapTotal']) * 100.00

  def eval_alert_thresholds(self):
    for item in self.data['evaluated']:
      # Check if we have something to evaluate
      if item in self.settings['alert_thresholds'] and self.settings['alert_thresholds'][item]:
        # Shorten dict chains
        td = self.settings['alert_thresholds'][item]
        val = self.data['evaluated'][item]

        # Evaluate
        alert = self.get_highest_threshold(val, td)

        # Create alert recommendations
        if alert:
          recommendation = {}
          recommendation['severity'] = alert
          recommendation['type'] = item
          recommendation['reporter'] = self.__class__.__name__
          recommendation['threshold_value'] = td[alert]
          recommendation['data'] = val
          self.zsm_obj.data['alerts'].append(recommendation)
            

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      if 'settings' in self.zsm_obj.conf['plugins'][self.__class__.__name__]:
        conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
        for section in conf:
          if isinstance(conf[section], dict):
            for subsection in conf[section]:
              if section in self.settings and subsection in self.settings[section]:
                self.settings[section][subsection] = conf[section][subsection]
          # If not a dict, assume the section is a single property to set.
          else:
            if section in self.settings:
              self.settings[section] = conf[section]
          

  ## Collect Data
  #
  # 
  def get_data(self):
    with open('/proc/meminfo') as f:
      self.data['collected']['memory'] = f.read()

  ## Evaluate Data
  #
  # 
  def evaluate_data(self):
    self.eval_memory()
    self.eval_alert_thresholds()
    if 'evaluated' in self.data and self.data['evaluated']:
      d = {}
      for key in self.data['evaluated']:
        if key == 'data':
          for k in self.data['evaluated'][key]:
            d[k] = self.data['evaluated'][key][k]
        elif self.data['evaluated'][key] or self.data['evaluated'][key] == 0:
          d[key] = self.data['evaluated'][key]
      self.create_report_data(d, 'zsm_mem_swap')

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    if self.settings['save_data']:
      self.zsm_obj.data['plugins'][self.__class__.__name__] = self.data['evaluated']
