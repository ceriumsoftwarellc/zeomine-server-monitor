import csv
from core.plugins.zsm_plugin import ZSMPlugin

## Linux System/Memory Load
#
# Records system load and memory usage. If certain thresholds are met, and alert
# threshold is met, and alert recommendation is sent.
#
class LinuxSystemLoad(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  settings = {}
  
  ## Alert Thresholds
  #
  # Load averages will vary by system.
  # Setting should be a dict of severity:threshold values. 
  # For example, if a system load of 2.0 is the max:
  #
  # load_1min: {'notice': 1.5, 'warning': 1.7, 'critical':1.9, 'highly_critical': 2.0}
  # Memory is calculated as a percentage of free memory from /proc/meminfo, and 
  # percentages should be given as a float, and a whole percentage, i.e. 20% = 20.0
  settings['alert_thresholds'] = {}
  settings['alert_thresholds']['load_1min'] = False
  settings['alert_thresholds']['load_5min'] = False
  settings['alert_thresholds']['load_15min'] = False

  settings['save_data'] = False

  # Local Data
  data = {}
  data['collected'] = {}
  data['evaluated'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################
  def get_highest_threshold(self, val, threshold_dict):
    ret = False
    for t in threshold_dict:
      if val > float(threshold_dict[t]):
        ret = t
      else:
        pass
    return ret

  def eval_load_average(self):
    load = self.data['collected']['loadavg'].split(' ')
    self.data['evaluated']['load_1min'] = float(load[0])
    self.data['evaluated']['load_5min'] = float(load[1])
    self.data['evaluated']['load_15min'] = float(load[2])

  def eval_alert_thresholds(self):
    for item in self.data['evaluated']:
      # Check if we have something to evaluate
      if item in self.settings['alert_thresholds'] and self.settings['alert_thresholds'][item]:
        # Shorten dict chains
        td = self.settings['alert_thresholds'][item]
        val = self.data['evaluated'][item]

        # Evaluate
        alert = False
        alert = self.get_highest_threshold(val, td)

        # Create alert recommendations
        if alert:
          recommendation = {}
          recommendation['severity'] = alert
          recommendation['type'] = item
          recommendation['reporter'] = self.__class__.__name__
          recommendation['threshold_value'] = td[alert]
          recommendation['data'] = val
          self.zsm_obj.data['alerts'].append(recommendation)
            

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      if 'settings' in self.zsm_obj.conf['plugins'][self.__class__.__name__]:
        conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
        for section in conf:
          if isinstance(conf[section], dict):
            for subsection in conf[section]:
              if section in self.settings and subsection in self.settings[section]:
                self.settings[section][subsection] = conf[section][subsection]
          # If not a dict, assume the section is a single property to set.
          else:
            if section in self.settings:
              self.settings[section] = conf[section]
          

  ## Collect Data
  #
  # 
  def get_data(self):
    with open('/proc/loadavg') as f:
      self.data['collected']['loadavg'] = f.read()

  ## Evaluate Data
  #
  # 
  def evaluate_data(self):
    self.eval_load_average()
    self.eval_alert_thresholds()
    if 'evaluated' in self.data and self.data['evaluated']:
      self.create_report_data(self.data['evaluated'], 'zsm_system_load')

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    if self.settings['save_data']:
      self.zsm_obj.data['plugins'][self.__class__.__name__] = self.data['evaluated']
