import smtplib
from core.plugins.zsm_plugin import ZSMPlugin
from email.mime.text import MIMEText

## Mail Alert Method
#
class MailAlert(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################

  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None

  # Settings dict
  # TODO: write this up:
  # recipients:
  #   __default__:
  #     - a@example.com
  #   notice:
  #     - c@example.com
  # components:
  #   from:
  #     type: fixed
  #     value: alerts@example.com
  #   subject:
  #     type: multiple
  #     value:
  #       1:
  #         type: fixed
  #         value: Fixed Text
  #       2:
  #         type: variable
  #         value: type
  #   body:
  #     type: multiple
  #     value:
  #       1:
  #         type: variable
  #         value: severity
  #       2:
  #         type: variable
  #         value: data
  settings = {}
  settings['recipients'] = {}
  settings['components'] = {}
  settings['server'] = 'localhost'

  # Local data dict
  data = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def generate_alert_component(self, alert, config_data):
    ret = ''
    if config_data['type'] == 'fixed':
      ret = str(config_data['value'])
    elif config_data['type'] == 'variable':
      ret = str(alert[config_data['value']])
    elif config_data['type'] == 'multiple':
      ret = ''
      for part in config_data['value']:
        add = self.generate_alert_component(alert, config_data['value'][part])
        if 'join' in config_data and config_data['join']:
          ret = config_data['join'].join([ret, add])
        else:
          ret = ' '.join([ret, add])
    if 'label' in config_data and config_data['label']:
      if config_data['type'] == 'fixed':
        label = str(config_data['label'])
      elif config_data['type'] == 'variable':
        label = str(config_data['value'])
      ret = config_data['join'].join([label, ret])
    return ret

  def mail(self, alert):
    config_base = {}
    if alert['severity'] in self.settings['components']:
      config_base = self.settings['components'][alert['severity']]
    else:
      config_base = self.settings['components']
    msg=MIMEText(self.generate_alert_component(alert, config_base['body']))
    frm = self.generate_alert_component(alert, config_base['from'])
    to_list = []
    if alert['severity'] in  self.settings['recipients']:
      to_list = self.settings['recipients'][alert['severity']]
    elif '__default__' in self.settings['recipients']:
      to_list = self.settings['recipients']['__default__']

    for to in to_list:
      msg["Subject"] = self.generate_alert_component(alert, config_base['subject'])
      msg["From"] = frm
      msg["To"] = to

      s = smtplib.SMTP(self.settings['server'])
      s.sendmail(frm,[to],msg.as_string())

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings:
              self.settings[section][subsection] = conf[section][subsection]
            else:
              self.settings[section] = {}
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          if section in self.settings:
            self.settings[section] = conf[section]

  # Authenticate with remote services
  #
  # TODO: if we need to authenticate with a mail server, do so here.
  def authenticate(self):
    pass

  ## Send Alerts
  #
  #
  def alert(self):
    for alert in self.zsm_obj.data['alerts']:
      self.mail(alert)

  ## Generate Reports and save data
  #
  # TODO: record sent mail
  def report(self):
    pass

  ## Final actions for ZSM shutdown
  #
  # TODO: save sent mail
  def shutdown(self):
    pass
