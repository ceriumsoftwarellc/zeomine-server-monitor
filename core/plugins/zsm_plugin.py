

## ZSM Generic Plugin
#
class ZSMPlugin():
  ##############################################################################
  # Plugin Settings
  ##############################################################################

  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None

  # Settings dict
  settings = {}

  # Local data dict
  data = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  ## Create an alert recommendation
  #
  # Standard thresholds are notice, warning, critical, and highly_critical.
  # You may create a custom severity to create extra user-specific cases.
  def create_alert(self, data, alert_type, threshold_value, severity = 'notice'):
    recommendation = {}
    recommendation['severity'] = severity
    recommendation['type'] = alert_type
    recommendation['threshold_value'] = threshold_value
    recommendation['data'] = data

    recommendation['reporter'] = self.__class__.__name__ # Usually the class name

    self.zsm_obj.data['alerts'].append(recommendation)


  ## Create a standard unit of report data
  #
  # Standard report data is structured to be ordered by type, reporter, 
  # and a dict of data.
  def create_report_data(self, data, report_type):
    report_data = {}
    report_data['type'] = report_type
    report_data['data'] = data

    report_data['reporter'] = self.__class__.__name__ # Usually the class name

    self.zsm_obj.data['report_data'].append(report_data)

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    pass

  ## Load Previous State for running comparisons
  #
  # 
  def load_previous_state(self):
    pass

  ## Collect Data
  #
  # 
  def get_data(self):
    pass

  ## Evaluate Data
  #
  # 
  def evaluate_data(self):
    pass

  # Authenticate with remote services
  #
  #
  def authenticate(self):
    pass

  ## Send Alerts
  #
  # 
  def alert(self):
    pass

  ## Generate Reports and save data
  #
  # 
  def report(self):
    pass

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    pass
