import json
import re
import os
import time
from core.plugins.zsm_plugin import ZSMPlugin
from datetime import datetime
from os import listdir
from os.path import isfile, join

## Backup Date
#
# Checks the date of the last backup 
#
class BackupDate(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  settings = {}
  
  # Backup patterns:
  # Each pattern is a dict with the key as the pattern name, with
  # the dict containing a 'location' which is the absolute path to the 
  # backup folder, and a 'pattern' which is a RegEx pattern that matches
  # the backup files.
  settings['backup_patterns'] = {}

  # Alerts
  # Alerts are keyed by the pattern names above, and should be dicts 
  # of the form [severity] : [age in seconds to trigger alert]
  # You may use the key __default as a catch-all if a pattern isn't
  # specified for an alert, or if you want only one policy for all items
  settings['alert_thresholds'] = {}

  # Local data dict
  data = {}
  data['backups'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################
  
  # Get the highest applicable threshold
  def get_highest_threshold(self, val, threshold_dict):
    ret = False
    for t in threshold_dict:
      if val > float(threshold_dict[t]):
        ret = t
      else:
        pass
    return ret
  
  # Create an alert recommendation for new commits
  def create_alert(self,data):
    recommendation = {}
    recommendation['severity'] = data['severity']
    recommendation['type'] = 'backup_date'
    recommendation['reporter'] = self.__class__.__name__
    recommendation['threshold_value'] = data['threshold']
    recommendation['data'] = data['data']
    self.zsm_obj.data['alerts'].append(recommendation)

  # Convert bytes to a readable value
  # source: https://stackoverflow.com/a/31631711
  def humanbytes(self, B):
    B = float(B)
    KB = float(1024)
    MB = float(KB ** 2) # 1,048,576
    GB = float(KB ** 3) # 1,073,741,824
    TB = float(KB ** 4) # 1,099,511,627,776
    if B < KB:
      return '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
    elif KB <= B < MB:
      return '{0:.2f} KB'.format(B/KB)
    elif MB <= B < GB:
      return '{0:.2f} MB'.format(B/MB)
    elif GB <= B < TB:
      return '{0:.2f} GB'.format(B/GB)
    elif TB <= B:
      return '{0:.2f} TB'.format(B/TB)

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings:
              self.settings[section][subsection] = conf[section][subsection]
            else:
              self.settings[section] = {}
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          self.settings[section] = conf[section]

  ## Collect Data
  #
  # 
  def get_data(self):
    compare_time = time.time()
    for pattern in self.settings['backup_patterns']:
      path = os.path.abspath(self.settings['backup_patterns'][pattern]['location'])
      files = [f for f in listdir(path) if isfile(join(path, f))]
      for fil in files:
        # Do the regex search to see if we have our file
        pat = re.compile(self.settings['backup_patterns'][pattern]['pattern'])
        matches = pat.search(fil)
        if matches:
          fp = path + '/' + fil
          t = 0
          if 'age' not in self.settings['backup_patterns'][pattern] or not self.settings['backup_patterns'][pattern]['age'] or self.settings['backup_patterns'][pattern]['age'] == 'created':
            t = os.path.getctime(fp)
          else:
            t = os.path.getmtime(fp)
          # Calculate the time between the backup and the present
          ti = int(time.time() - t)
          # We have the age, save it to our data
          if not pattern in self.data['backups'] or ti < self.data['backups'][pattern]['age']:
            self.data['backups'][pattern] = {'age': ti, 'file': fil}
            # Add additional data about the file
            self.data['backups'][pattern]['date'] = datetime.utcfromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')
            self.data['backups'][pattern]['size'] = self.humanbytes(os.path.getsize(fp))
          

  ## Evaluate Data
  #
  # 
  def evaluate_data(self):
    for pattern in self.data['backups']:
      # Check if we have alert thresholds set
      if 'alert_thresholds' in self.settings and self.settings['alert_thresholds']:
        # Check to see if we have any alerts
        th = False
        th_val = 0
        if pattern in self.settings['alert_thresholds']:
          th = self.get_highest_threshold(self.data['backups'][pattern]['age'], self.settings['alert_thresholds'][pattern])
          th_val = self.settings['alert_thresholds'][pattern][th] if th else 0
        elif '__default' in self.settings['alert_thresholds']:
          th = self.get_highest_threshold(self.data['backups'][pattern]['age'], self.settings['alert_thresholds']['__default'])
          th_val = self.settings['alert_thresholds']['__default'][th] if th else 0
        if th:
          alert_data = {'severity': th, 'data': self.data['backups'][pattern]['age'], 'threshold': th_val}
          self.create_alert(alert_data)
      # Generate data for reporting
      rd = self.data['backups'][pattern]
      rd['pattern'] = pattern.strip()
      self.create_report_data(rd, 'zsm_backup_date')
      

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    pass
