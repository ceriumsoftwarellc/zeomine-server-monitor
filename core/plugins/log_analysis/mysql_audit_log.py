import hashlib
import json
import re
import time
from datetime import datetime
from core.plugins.zsm_plugin import ZSMPlugin

## Access Log
#
# Track access log items and report them
#
class MySQLAuditLog(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  #
  # access_log_path: Absolute path to the access log file, with filename.
  # access_log_state:
  #   filename: Filename of the state file, which tracks whether an
  #     item has already been sent.
  #   max_age: Drop items from the state file if they are older than
  #     this amount, in seconds. Put 0 to save forever, 86400 = 1 day.
  settings = {}
  settings['log_path'] = ''
  settings['log_state'] = {}
  settings['log_state']['filename'] = 'mysql_audit_log_state.json'
  settings['log_state']['max_age'] = 86400
  settings['log_state']['save_state'] = True
  
  ## Reports
  #
  # type: generate report data per transaction, or bundle transactions
  # per connection ID, per second, per minute, or per hour
  #
  # keys: These are the data keys in each transaction entry
  # additional: These are custom reports specific to each report type.
  # Some are consistent across multiple types:
  #  - table: Collect the table referenced in a transaction. Does not
  #    include joins. Used in second/minute/hour reports
  #  - duration: Connection duration. For connection report only
  #  - timestamp: Connection timestamp. For connection report only, as
  #    second/minute/hour reports will have the "timestamp" set as the
  #    time of the minute or hour.
  settings['report'] = {}
  settings['report']['type'] = ['hour', 'minute', 'second', 'connection', 'transaction']
  settings['report']['hour'] = {}
  settings['report']['hour']['keys'] = ['db', 'name', 'ip', 'command_class', 'connection_id']
  settings['report']['hour']['additional'] = [] # 'table'
  settings['report']['minute'] = {}
  settings['report']['minute']['keys'] = ['db', 'name', 'ip', 'command_class', 'connection_id']
  settings['report']['minute']['additional'] = [] # 'table'
  settings['report']['second'] = {}
  settings['report']['second']['keys'] = ['db', 'name', 'ip', 'command_class', 'connection_id']
  settings['report']['second']['additional'] = [] # 'table'
  # Most connection keys report as a count, except connection_id, 
  # which reports as a string of the ID
  settings['report']['connection_raw'] = {}
  settings['report']['connection_raw']['keys'] = ['db', 'name', 'ip', 'command_class', 'connection_id']
  settings['report']['connection_raw']['additional'] = [] # 'table', 'duration', 'timestamp'
  settings['report']['transaction'] = {}
  settings['report']['transaction']['keys'] = ['db', 'name', 'ip', 'command_class']

  # Currently we have no alert settings. The system reports log data only.
  # settings['alerts'] = {}

  # Local data dict
  #
  # Items are stored with a hash key + the data. There is also a "reported"
  # variable to denote whether the item has been made into a report yet.
  data = {}
  data['transaction'] = {}
  data['connection_raw'] = {}
  data['connection'] = {}
  data['second'] = {}
  data['minute'] = {}
  data['hour'] = {}
  data['log_state'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################
  
  # Determine whether an entry has been added to the data set
  def is_reported(self, hsh):
    try:
      this = self.data['log_state'][hsh]
      return True
    except:
      return False

  # Determine whether an entry has been added to the data set
  def is_in_connection_data(self, connection_id, hsh):
    if connection_id in self.data['connection_raw'] and self.data['connection_raw'][connection_id]:
      if hsh in self.data['connection_raw'][connection_id] and self.data['connection_raw'][connection_id][hsh]:
        return True
      else:
        return False
    else:
      return False
  
  # Digest an access log entry
  #
  # Assumes the entry is a single line string
  def digest_entry(self, item, t):
    hsh = hashlib.sha256(str(item).encode()).hexdigest()
    if not self.is_reported(hsh):
      try:
        connect_id = item['audit_record']['connection_id']      
        itm = {}
        item = item['audit_record']
        self.data['transaction'][hsh] = {}
        for k in item:
          self.data['transaction'][hsh][k] = item[k]
          if k == 'timestamp':
            t = datetime.strptime(item['timestamp'], "%Y-%m-%dT%H:%M:%S UTC")
            t = round(t.timestamp())
            self.data['transaction'][hsh]['timestamp_processed'] = t
        if 'connection' in self.settings['report']['type'] and 'connection_id' in item and item['connection_id'] and not self.is_in_connection_data(item['connection_id'], hsh):
          if not item['connection_id'] in self.data['connection_raw']:
            self.data['connection_raw'][item['connection_id']] = []
          self.data['connection_raw'][item['connection_id']].append(hsh)
      except:
        pass

  def generate_connection_report_data(self, data_list, connect_id):
    report_item = {'connection_id': connect_id, 'query_count': 0}
    time_data = {'min': 0, 'max': 0, 'date': ''}
    for item in data_list:
      if 'name' in self.data['transaction'][item] and self.data['transaction'][item]['name'] == 'Query':
        report_item['query_count'] += 1
        for key in self.data['transaction'][item]:
          if key in self.settings['report']['connection_raw']['keys']:
            if key not in report_item:
              report_item[key] = {'total': 1, self.data['transaction'][item][key]: 1}
            elif self.data['transaction'][item][key] not in report_item[key]:
              report_item[key]['total'] += 1
              report_item[key][self.data['transaction'][item][key]] = 1
            else:
              report_item[key]['total'] += 1
              report_item[key][self.data['transaction'][item][key]] += 1
      if 'timestamp' in self.settings['report']['connection_raw']['additional'] or \
      'duration' in self.settings['report']['connection_raw']['additional']:
        if 'timestamp_processed' in self.data['transaction'][item]:
          t = self.data['transaction'][item]['timestamp_processed']
          if not time_data['min'] or t < time_data['min']:
            time_data['min'] = t
            time_data['date'] = self.data['transaction'][item]['timestamp']
          if not time_data['max'] or t > time_data['max']:
            time_data['max'] = t
    if 'timestamp' in self.settings['report']['connection_raw']['additional']:
      report_item['timestamp'] = time_data['min']
      report_item['date'] = time_data['date']
    if 'duration' in self.settings['report']['connection_raw']['additional']:
      report_item['duration'] = time_data['max'] - time_data['min']
    self.data['connection'][connect_id] = report_item
    return report_item

  # General function for time series statistics
  def generate_time_series_data(self, timestamp, transaction, series_type):
    ts = int(timestamp)
    if timestamp not in self.data[series_type]:
      self.data[series_type][timestamp] = {'timestamp': timestamp,'timestamp_formatted': datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')}
    for key in transaction:
      if key in self.settings['report'][series_type]['keys']:
        if key not in self.data[series_type][timestamp]:
          self.data[series_type][timestamp][key] = {'total': 1, transaction[key]: 1}
        elif transaction[key] not in self.data[series_type][timestamp][key]:
          self.data[series_type][timestamp][key]['total'] += 1
          self.data[series_type][timestamp][key][transaction[key]] = 1
        else:
          self.data[series_type][timestamp][key]['total'] += 1
          self.data[series_type][timestamp][key][transaction[key]] += 1
    if 'table' in self.settings['report'][series_type]['additional']:
      if 'table' not in self.data[series_type][timestamp]:
        self.data[series_type][timestamp]['table'] = {}
      if 'sqltext' in transaction:
        command = transaction['command_class']
        table = re.findall('(?:INSERT\sINTO|UPDATE|FROM)\W+(\w+)' , transaction['sqltext'])
        if table:
          table = table[0]
          if table not in self.data[series_type][timestamp]['table']:
            self.data[series_type][timestamp]['table'][table] = {}
            self.data[series_type][timestamp]['table'][table]['table'] = table
            self.data[series_type][timestamp]['table'][table]['total'] = 1
            self.data[series_type][timestamp]['table'][table]['select'] = 0
            self.data[series_type][timestamp]['table'][table]['insert'] = 0
            self.data[series_type][timestamp]['table'][table]['update'] = 0
            self.data[series_type][timestamp]['table'][table]['delete'] = 0
            self.data[series_type][timestamp]['table'][table][command] = 1
          elif command not in self.data[series_type][timestamp]['table'][table]:
            self.data[series_type][timestamp]['table'][table]['total'] += 1
            self.data[series_type][timestamp]['table'][table][command] = 1
          else:
            self.data[series_type][timestamp]['table'][table]['total'] += 1
            self.data[series_type][timestamp]['table'][table][command] += 1

  # Per-second statistics
  def generate_second_report_data(self):
    for hsh in self.data['transaction']:
      t = str(self.data['transaction'][hsh]['timestamp_processed'])
      self.generate_time_series_data(t, self.data['transaction'][hsh], 'second')

  
  # Per-minute statistics
  def generate_minute_report_data(self):
    for hsh in self.data['transaction']:
      t = self.data['transaction'][hsh]['timestamp_processed']
      ts = datetime.utcfromtimestamp(t).strftime('%Y-%m-%d %H:%M')
      t = datetime.strptime(ts, '%Y-%m-%d %H:%M')
      t = round(t.timestamp())
      self.generate_time_series_data(t, self.data['transaction'][hsh], 'minute')
  
  # Per-hour statistics
  def generate_hour_report_data(self):
    for hsh in self.data['transaction']:
      t = self.data['transaction'][hsh]['timestamp_processed']
      ts = datetime.utcfromtimestamp(t).strftime('%Y-%m-%d %H')
      t = datetime.strptime(ts, '%Y-%m-%d %H')
      t = round(t.timestamp())
      self.generate_time_series_data(t, self.data['transaction'][hsh], 'hour')

  ## Report Time Series Data (second/minute/hour)
  #
  def report_time_series_data(self, time_unit):
    for item in self.data[time_unit]:
      if self.data[time_unit][item]:
        self.create_report_data(self.data[time_unit][item], 'mysql_audit_log_' + time_unit)
        if 'table' in self.settings['report'][time_unit]['additional']:
          for table in self.data[time_unit][item]['table']:
            if self.data[time_unit][item]['table'][table]:
              self.data[time_unit][item]['table'][table]['timestamp'] = self.data[time_unit][item]['timestamp']
              self.data[time_unit][item]['table'][table]['timestamp_formatted'] = self.data[time_unit][item]['timestamp_formatted']
              self.create_report_data(self.data[time_unit][item]['table'][table], 'mysql_audit_log_' + time_unit+ '_table_' + table)

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings and subsection in self.settings[section]:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          if section in self.settings:
            self.settings[section] = conf[section]

  ## Load Previous State for running comparisons
  #
  # 
  def load_previous_state(self):
    try:
      with open(self.zsm_obj.settings['user_data']['data'] + self.settings['log_state']['filename'], 'r') as f:
        r = json.load(f)
        current_time = time.time()
        i = 0
        for hsh in r:
          if i % 10000 == 0:
            self.zsm_obj.pprint('Read ' + str(i) + ' items from log state file')
          i += 1
          if self.settings['log_state']['max_age'] and (current_time - r[hsh]) > self.settings['log_state']['max_age']:
            pass
          else:
            self.data['log_state'][hsh] = r[hsh]
    except:
      self.zsm_obj.pprint("The Audit Log state file " + self.zsm_obj.settings['user_data']['data'] + self.settings['log_state']['filename'] + " was not found.",0)

  ## Collect Data
  #
  # 
  def get_data(self):
    with open(self.settings['log_path']) as f:
      t = time.time()
      i = 1
      for line in f.readlines():
        l = json.loads(line)
        self.digest_entry(l, t)
        if i % 10000 == 0:
          self.zsm_obj.pprint('Read ' + str(i) + ' items')
        i += 1

  ## Evaluate Data
  #
  # 
  def evaluate_data(self):
    if 'transaction' in self.settings['report']['type']:
      for hsh in self.data['transaction']:
        self.create_report_data(self.data['transaction'][hsh], 'mysql_audit_log_transaction')
    if 'connection_raw' in self.settings['report']['type']:
      for connect_id in self.data['connection_raw']:
        report_data = self.generate_connection_report_data(self.data['connection_raw'][connect_id], connect_id)
        self.create_report_data(report_data, 'mysql_audit_log_connection')
    if 'second' in self.settings['report']['type']:
      self.generate_second_report_data()
      self.report_time_series_data('second')
    if 'minute' in self.settings['report']['type']:
      self.generate_minute_report_data()
      self.report_time_series_data('second')
    if 'hour' in self.settings['report']['type']:
      self.generate_hour_report_data()
      self.report_time_series_data('hour')
    # Finally, add reported items to the finished items list
    t = time.time()
    for hsh in self.data['transaction']:
      self.data['log_state'][hsh] = t

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    with open(self.zsm_obj.settings['user_data']['data'] + self.settings['log_state']['filename'], 'w') as f:
      w = json.dumps(self.data['log_state'])
      f.write(w)
