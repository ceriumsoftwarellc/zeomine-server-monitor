import re
import json
from core.plugins.zsm_plugin import ZSMPlugin

## Git Commit Log
#
# Track git commits and report/alert on 
#
class GitCommitLog(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  settings = {}
  settings['git_log_path'] = ''
  settings['git_state_path'] = 'gitstate.json'

  settings['alerts'] = {}
  settings['alerts']['new_commit_severity'] = False

  # Local data dict
  data = {}
  data['commits'] = {}
  data['new_commits'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################
  
  # Create an alert recommendation for new commits
  def create_alert(self,commit_id):
    recommendation = {}
    recommendation['severity'] = self.settings['alerts']['new_commit_severity']
    recommendation['type'] = 'new commit'
    recommendation['reporter'] = self.__class__.__name__
    recommendation['threshold_value'] = 'new commit'
    recommendation['data'] = commit_id
    self.zsm_obj.data['alerts'].append(recommendation)

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings and subsection in self.settings[section]:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          if section in self.settings:
            self.settings[section] = conf[section]

  ## Load Previous State for running comparisons
  #
  # 
  def load_previous_state(self):
    try:
      with open(self.zsm_obj.settings['user_data']['data'] + self.settings['git_state_path'], 'r') as f:
        r = json.load(f)
        for commit_id in r:
          self.data['commits'][commit_id] = r[commit_id]
    except:
      self.zsm_obj.pprint("The Git state file " + self.zsm_obj.settings['user_data']['data'] + self.settings['git_state_path'] + " was not found.",0)

  ## Collect Data
  #
  # 
  def get_data(self):
    with open(self.settings['git_log_path']) as f:
      for line in f.readlines():
        sp_split = line.split(' ')
        # Check if there is a commit/change
        if sp_split[0] != sp_split[1] and sp_split[1] not in self.data['commits']:
          commit_id = sp_split[1]
          self.data['commits'][commit_id] = {}
          # Commits
          self.data['commits'][commit_id]['id'] = commit_id
          self.data['commits'][commit_id]['previous'] = sp_split[0]
          # Time
          t = re.search('(\d+\s-\d+)' , line)
          if t:
            ti = t.group(0)
            self.data['commits'][commit_id]['time'] = ti
            tim = ti.split(' ')
            self.data['commits'][commit_id]['timestamp'] = tim[0]
          # Commit Message
          t_split = line.split('\t')
          self.data['commits'][commit_id]['message'] = t_split[1].strip()
          # Name and Email
          i = 2
          self.data['commits'][commit_id]['name'] = ''
          while i < len(sp_split) - 1:
            if '<' not in sp_split[i] and '@' not in sp_split[i]:
              self.data['commits'][commit_id]['name'] += sp_split[i] if not self.data['commits'][commit_id]['name'] else ' ' + sp_split[i]
            else:
              self.data['commits'][commit_id]['email'] = sp_split[i].strip('<').strip('>')
              break
            i += 1
          # Finally, alert that a new commit was found, and add to the new commit list
          self.data['new_commits'][commit_id] = self.data['commits'][commit_id]
          if self.settings['alerts']['new_commit_severity']:
            self.create_alert(commit_id)
          # Also, create a report data
          self.create_report_data(self.data['new_commits'][commit_id], 'zsm_git_commit_log')
      #~ self.zsm_obj.pprint(self.data['commits'],0)

  ## Evaluate Data
  #
  # 
  def evaluate_data(self):
    pass

  ## Generate Reports and save data
  #
  # 
  def report(self):
    pass

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    with open(self.zsm_obj.settings['user_data']['data'] + self.settings['git_state_path'], 'w') as f:
      w = json.dumps(self.data['commits'])
      f.write(w)
