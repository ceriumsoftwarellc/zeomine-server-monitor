import hashlib
import json
import re
import time
from core.plugins.zsm_plugin import ZSMPlugin

## Access Log
#
# Track access log items and report them
#
class AccessLog(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  #
  # access_log_path: Absolute path to the access log file, with filename.
  # access_log_state:
  #   filename: Filename of the state file, which tracks whether an
  #     item has already been sent.
  #   max_age: Drop items from the state file if they are older than
  #     this amount, in seconds. Put 0 to save forever.
  settings = {}
  settings['access_log_path'] = ''
  settings['access_log_state'] = {}
  settings['access_log_state']['filename'] = 'accesslog_state.json'
  settings['access_log_state']['max_age'] = 0
  settings['access_log_state']['save_state'] = True

  # Currently we have no alert settings. The system reports log data only.
  # settings['alerts'] = {}

  # Local data dict
  #
  # Items are stored with a hash key + the data. There is also a "reported"
  # variable to denote whether the item has been made into a report yet.
  data = {}
  data['items'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################
  
  # Create an alert recommendation for new commits
  def create_alert(self,commit_id):
    recommendation = {}
    recommendation['severity'] = self.settings['alerts']['new_commit_severity']
    recommendation['type'] = 'new commit'
    recommendation['reporter'] = self.__class__.__name__
    recommendation['threshold_value'] = 'new commit'
    recommendation['data'] = commit_id
    self.zsm_obj.data['alerts'].append(recommendation)

  # Determine whether an entry has already been reported
  def is_reported(self, hsh):
    if self.is_in_data(hsh):
      if 'reported' in self.data['items'][hsh] and self.data['items'][hsh]['reported']:
        return True
      else:
        return False
    else:
      return False
  
  # Determine whether an entry has been added to the data set
  def is_in_data(self, hsh):
    if hsh in self.data['items'] and self.data['items'][hsh]:
      return True
    else:
      return False
  
  # Digest an access log entry
  #
  # Assumes the entry is a single line string
  def digest_entry(self, item, t):
    hsh = hashlib.sha256(str(item).encode()).hexdigest()
    if not self.is_in_data(hsh):
      try:
        itm = {}
        ip = item.split(' ', 1)
        itm['ip'] = ip[0]
        usr1 = ip[1].split(' ', 1)
        itm['usr1'] = usr1[0]
        usr2 = usr1[1].split(' ', 1)
        itm['usr2'] = usr2[0]
        tim = usr2[1].split('] ', 1)
        itm['time'] = tim[0].strip('[')
        verb = tim[1].split(' ', 1)
        itm['verb'] = verb[0].strip('"')
        url = verb[1].split(' ', 1)
        itm['url'] = url[0]
        http = url[1].split(' ', 1)
        itm['http'] = http[0].strip('"')
        status = http[1].split(' ', 1)
        itm['status'] = status[0]
        duration = status[1].split(' ', 1)
        itm['duration'] = duration[0]
        referrer = duration[1].split(' ', 1)
        itm['referrer'] = referrer[0]
        agent = referrer[1].strip('"').strip()
        itm['agent'] = agent
        # Add metadata, and add the constructed item to the data
        itm['log_metadata'] = {}
        itm['log_metadata']['record_time'] = t
        itm['log_metadata']['reported'] = False
        self.data['items'][hsh] = itm
      except:
        pass

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings and subsection in self.settings[section]:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          if section in self.settings:
            self.settings[section] = conf[section]

  ## Load Previous State for running comparisons
  #
  # 
  def load_previous_state(self):
    try:
      with open(self.zsm_obj.settings['user_data']['data'] + self.settings['access_log_state']['filename'], 'r') as f:
        r = json.load(f)
        current_time = time.time()
        for hsh in r:
          if self.settings['access_log_state']['max_age'] and (current_time - r[hsh]['log_metadata']['record_time']) > self.settings['access_log_state']['max_age']:
            pass
          else:
            self.data['items'][hsh] = r[hsh]
    except:
      self.zsm_obj.pprint("The Access Log state file " + self.zsm_obj.settings['user_data']['data'] + self.settings['access_log_state']['filename'] + " was not found.",0)

  ## Collect Data
  #
  # 
  def get_data(self):
    with open(self.settings['access_log_path']) as f:
      t = time.time()
      for line in f.readlines():
        self.digest_entry(line, t)

  ## Evaluate Data
  #
  # 
  def evaluate_data(self):
    for hsh in self.data['items']:
      item = self.data['items'][hsh]
      if not item['log_metadata']['reported']:
        itm = {}
        for data in item:
          if data != 'log_metadata':
            itm[data] = item[data]
        self.create_report_data(itm, 'zsm_access_log')
        item['log_metadata']['reported'] = True

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    with open(self.zsm_obj.settings['user_data']['data'] + self.settings['access_log_state']['filename'], 'w') as f:
      w = json.dumps(self.data['items'])
      f.write(w)
