import json
import importlib
import requests
import time
from core.plugins.zsm_plugin import ZSMPlugin

## Remote Configuration
#
class RemoteConf(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################

  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None

  # Settings dict
  settings = {}
  settings['cache'] = {}
  settings['cache']['cache_conf'] = False
  settings['cache']['cache_expire'] = 86400
  settings['cache']['cache_filename'] = 'conf_cache.json'

  settings['conf_url'] = ''

  settings['auth'] = {}
  settings['auth']['location'] = ''
  settings['auth']['name'] = ''
  settings['auth']['pass'] = ''

  # Local data dict
  data = {}
  data['remote_conf_loaded'] = False
  data['auth'] = {}
  data['cache_time'] = False
  data['remote_donf'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def login(self):
    login = {}
    login['name'] = self.settings['auth']['name']
    login['pass'] = self.settings['auth']['pass']
    l = json.dumps(login)

    req = requests.post(self.settings['auth']['location'], data=l)

    self.data['auth']['head'] = {}
    self.data['auth']['cookie'] = {}
    if req.status_code == 200:
      r = json.loads(req.text)
      self.data['auth']['head']['X-CSRF-Token'] = r['csrf_token']
      self.data['auth']['head']['Accept'] = 'application/json'
      self.data['auth']['head']['Content-Type'] = 'application/json'
      self.data['auth']['cookie'] = req.cookies.get_dict()
      return True
    else:
      self.zsm_obj.pprint("RemoteConf Login failed. Message from Server:",0)
      self.zsm_obj.pprint(req.text,0)
      return False


  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    if not self.data['remote_conf_loaded']:
      # Like most plugins, load the known config first
      if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
        conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
        for section in conf:
          if isinstance(conf[section], dict):
            for subsection in conf[section]:
              if section in self.settings and subsection in self.settings[section]:
                self.settings[section][subsection] = conf[section][subsection]
          # If not a dict, assume the section is a single property to set.
          else:
            if section in self.settings:
              self.settings[section] = conf[section]
      # Get the settings data, and apply it to the parent ZSM object
      remote_conf = {}
      if self.settings['cache']['cache_conf']:
        try:
          with open(self.zsm_obj.settings['user_data']['data'] + settings['cache']['cache_filename'], 'r') as f:
            cache_data = json.load(f)
            if cache_data['time'] + settings['cache']['cache_expire'] > time.time():
              remote_conf = cache_data['remote_conf']
            else:
              pass
        except:
          pass

      # Get the config from the site if we didn't get any remote data
      if not remote_conf:
        login_successful = self.login()
        if login_successful:
          rc = requests.get(self.settings['conf_url'], headers = self.data['auth']['head'], cookies = self.data['auth']['cookie'])
          if rc.status_code == 200:
            remote_conf = json.loads(rc.text)
          else:
            self.zsm_obj.pprint('Failed to fetch remote configuration', 0)
            self.zsm_obj.pprint(rc.text, 0)
        else:
          self.zsm_obj.pprint('Failed to log into remote config server', 0)

      # Write the cache data
      if self.settings['cache']['cache_conf']:
        with open(self.zsm_obj.settings['user_data']['data'] + self.settings['cache']['cache_filename'], 'w') as f:
          if 'cache_time' not in self.settings:
            self.settings['cache_time'] = time.time()
          cache_data = {'time': self.settings['cache_time'],'remote_conf': remote_conf}
          write_data = json.dumps(cache_data)
          f.write(write_data)

      # Finally, apply the found data and re-run the load_config function for ZSM, but exclude RemoteConf
      self.data['remote_conf_loaded'] = True
      self.zsm_obj.pprint(remote_conf, -1)
      for setting in remote_conf['settings']:
        if remote_conf['settings'][setting] or remote_conf['settings'][setting] == 0:
          self.zsm_obj.settings[setting] = remote_conf['settings'][setting]
      for conf in remote_conf['conf']:
        if remote_conf['conf'][conf] or remote_conf['conf'][conf] == 0:
          self.zsm_obj.conf[conf] = remote_conf['conf'][conf]
      # Output the remote config if we are debugging
      self.zsm_obj.pprint('** Remote Settings **', 2,2)
      self.zsm_obj.pprint(remote_conf['settings'], 2,2)
      self.zsm_obj.pprint('** Remote Configs**', 2,2)
      self.zsm_obj.pprint(remote_conf['conf'], 2,2)

      # Load the plugins, but not ourselves
      for plug in self.zsm_obj.conf['plugins']:
        if self.zsm_obj.conf['plugins'][plug]['class'] != 'RemoteConf':
          self.zsm_obj.pprint("Loading plugin " + plug, 1, 1)
          plugin_bases = {"core": "core.plugins", "sample": "samples", "custom": "user_data.plugins"}
          p = self.zsm_obj.conf['plugins'][plug]
          c = self.zsm_obj.conf['plugins'][plug]['class']
          m = importlib.import_module(plugin_bases[p['type']] + '.' + p['module'])
          plugin = getattr(m,c)
          # Check to see if we already added this plugin
          chk = []
          for plu in self.zsm_obj.plugins:
            chk.append(str(plu.__class__))
          if str(c) not in chk:
            # We can add this plugin. Also, set the zsm_obj
            self.zsm_obj.plugins[c] = plugin()
            self.zsm_obj.plugins[c].zsm_obj = self.zsm_obj

      # Show debug text of the settings, if we are verbose.
      self.zsm_obj.pprint('RemoteConf finished loading. These are the current ZSM Settings:', 2)
      self.zsm_obj.pprint('*** ZSM Settings ***', 2)
      self.zsm_obj.pprint(self.zsm_obj.settings, 2)
      self.zsm_obj.pprint('*** Plugin Settings ***', 2)
      for c in self.zsm_obj.conf['plugins']:
        self.zsm_obj.pprint(c, 2)
        self.zsm_obj.pprint(self.zsm_obj.conf['plugins'][c], 2)

      # Lets call the load_config method for each plugin, so they can carry out their startup actions.
      for plugin in self.zsm_obj.plugins:
        if callable(getattr(self.zsm_obj.plugins[plugin], 'load_config', False)):
          self.zsm_obj.plugins[plugin].load_config()

      # Finally, since we overrode the load command, enter a "stop" command to prevent conflicts
      # with the parent ZSM object.
      return True
