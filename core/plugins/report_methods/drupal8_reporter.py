import requests
import json
from core.plugins.zsm_plugin import ZSMPlugin

## Drupal 8 Reporter
#
class Drupal8Reporter(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  settings = {}

  settings['save_data'] = True

  settings['alert_thresholds'] = {}
  settings['alert_thresholds']['failed_upload'] = False

  settings['report'] = {}
  settings['report']['base_url'] = ''
  settings['report']['map'] = {}

  settings['auth'] = {}
  settings['auth']['location'] = ''
  settings['auth']['name'] = ''
  settings['auth']['pass'] = ''

  # Data dict
  data = {}
  data['auth'] = {}
  data['report'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def get_multipart_variable_keys(self, var_data):
    ret_data = ret = self.varbase(var_data, 'item_base')
    # Now, reach into the dict values
    varvalue = var_data['item_value'].split('.')
    for section in varvalue:
      ret_data = ret_data[section]
    # Get the keys
    ret_items = []
    for item in ret_data:
      ret_items.append(item)
    return ret_items

  def get_variable_data(self, var_data, key = None):
    if var_data['data'] == 'key' and key:
      return [{var_data['field_key']: str(key)}]
    elif var_data['data'] == 'fixed':
      return [{var_data['field_key']: str(var_data['value'])}]
    elif var_data['data'] == 'variable':
      ret = self.varbase(var_data)
      # Now, reach into the dict values
      varvalue = var_data['value'].split('.')
      for section in varvalue:
        ret = ret[section]
      return [{var_data['field_key']: str(ret)}]
    elif var_data['data'] == 'variable_key' and key:
      ret = self.varbase(var_data)
      # Now, get the part before the key
      varvalue = var_data['value_before'].split('.')
      for section in varvalue:
        ret = ret[section]
      # Next, get the key and the part afterward
      ret = ret[key]
      varvalue = var_data['value_after'].split('.')
      for section in varvalue:
        ret = ret[section]
      return [{var_data['field_key']: str(ret)}]
    else:
      return None

  def varbase(self, var_data, basekey = 'variable_base'):
    ret = None
    # Get the variable base first. This should always be property-based i.e. x.y.z
    # 'x' in x.y.z should always be 'zsm', 'self', or 'plugins' folloed by a plugin name
    varbase = var_data[basekey].split('.')
    root = varbase.pop(0)
    if root == 'zsm':
      ret = self.zsm_obj
    elif root == 'self':
      ret = self
    elif root == 'plugins':
      plugin = varbase.pop(0)
      ret = self.zsm_obj.plugins[plugin]
    # Get the properties
    for prop in varbase:
      ret = getattr(ret, prop)
    return ret

  def get_reportable_data(self):
    for item in self.settings['report']['map']:
      self.data['report'][item] = {}
      self.data['report'][item]['endpoint'] = self.settings['report']['base_url'] + self.settings['report']['map'][item]['endpoint']
      self.data['report'][item]['data'] = []
      # If there are multiple items, call the multi-item logic
      if 'multiple' in self.settings['report']['map'][item] and self.settings['report']['map'][item]['multiple']:
        keys = self.get_multipart_variable_keys(self.settings['report']['map'][item])
        for key in keys:
          # Go field by field
          add = {}
          for field in self.settings['report']['map'][item]['fields']:
            add[field] = self.get_variable_data(self.settings['report']['map'][item]['fields'][field], key)
          self.data['report'][item]['data'].append(add)
          
      # For a single item, we will still add it to the "data" list, albeit as one item
      else:
        # Go field by field
        add = {}
        for field in self.settings['report']['map'][item]['fields']:
          add[field] = self.get_variable_data(self.settings['report']['map'][item]['fields'][field])
        self.data['report'][item]['data'].append(add)

  def post_reportable_data(self):
    for item in self.settings['report']['map']:
      for add in self.data['report'][item]['data']:
        aj = json.dumps(add)
        p = requests.post(self.data['report'][item]['endpoint'], headers = self.data['auth']['head'], cookies = self.data['auth']['cookie'], data = aj)

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings and subsection in self.settings[section]:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          if section in self.settings:
            self.settings[section] = conf[section]

  # Authenticate with remote services
  #
  #
  def authenticate(self):
    login = {}
    login['name'] = self.settings['auth']['name']
    login['pass'] = self.settings['auth']['pass']
    l = json.dumps(login)

    req = requests.post(self.settings['auth']['location'], data=l)

    self.data['auth']['head'] = {}
    self.data['auth']['cookie'] = {}

    if req.status_code == 200:
      r = json.loads(req.text)
      self.data['auth']['head']['X-CSRF-Token'] = r['csrf_token']
      self.data['auth']['head']['Accept'] = 'application/json'
      self.data['auth']['head']['Content-Type'] = 'application/json'
      self.data['auth']['cookie'] = req.cookies.get_dict()

  ## Generate Reports and save data
  #
  # 
  def report(self):
    self.get_reportable_data()
    self.post_reportable_data()

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    if self.settings['save_data']:
      self.data.pop('auth', None)
      self.zsm_obj.data['plugins'][self.__class__.__name__] = self.data
