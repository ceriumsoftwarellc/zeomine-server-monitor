import requests
import json
from core.plugins.zsm_plugin import ZSMPlugin

## Drupal 8 Reporter
#
class SpectraReporter(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  settings = {}

  # Save data related to the module
  # TODO determine if we actually need this - maybe if we ask the server for additional actor, etc. data
  settings['save_data'] = True
  settings['save_data_path'] = '*.json'

  ## Report Settings
  #
  # url: URL to POST statements to
  # plugin_name: The recommended Spectra Plugin. May be blank to use the default
  # types: list with "alert", "report", or both
  # require/exclude: Post only the required items, or exclude the excluded items
  #   These filter by the "type" property of reporting data, which is set when 
  #   pluign.create_report_data() is called.
  #
  # Spectra Components
  #
  # Each component is a dict with keys of "type," which is "name" or "uuid,"
  # and the other key is "name" or "uuid" respectively.
  # The verb will always be "zsm_alert" or "zsm_report" depending on 
  # which is reported, and the data is the respective report/alert data.
  # You may also add other data in the expected format for your Spectra Plugin.
  # For example, you can add extra data to the actor via a "data" key,
  # or specify the Spectra actor type with a "type" key
  # TODO: enable uuid recognition
  # 
  # actor: The Spectra Actor
  # object: The Spectra Object
  # context: The Spectra Context
  settings['report'] = {}
  settings['report']['url'] = ''
  settings['report']['plugin_name'] = ''
  settings['report']['report_alerts'] = False
  settings['report']['report_reports'] = False
  settings['report']['require'] = []
  settings['report']['exclude'] = []
  settings['report']['actor'] = {'property_type': 'name', 'property': ''}
  settings['report']['object'] = {'property_type': 'name', 'property': ''}
  settings['report']['context'] = {'property_type': 'name', 'property': ''}
  settings['report']['statement_type'] = 'zsm_data'
  
  ## Data Groups
  #
  # Data of a similar type or set of types can be reported as a single 
  # data entity. This permits straightforward charting as XY data can 
  # be found in the same data object. This grouping does not apply to
  # alerts.
  #
  # Format: group_id: [key1, key2, ...]
  #
  # group_id: this is the key that will be used as the data type
  # keyX : a list of data keys that correspond to the "type" in report
  # objects. If you have multiples of this key, it will be overwritten
  # such that only the last item shows.
  settings['report']['group'] = {}
  
  ## Unique Items
  #
  # If you want to report a grouped item as a separate, unique item also,
  # put its type key here.
  settings['report']['unique'] = []

  # Auth data - use 'location', 'name', and 'pass' for a Drupal login,
  # or 'api_key' to use Drupal's key_auth module. Setting 'api_key' 
  # will override the login method.
  settings['auth'] = {}
  settings['auth']['location'] = ''
  settings['auth']['name'] = ''
  settings['auth']['pass'] = ''
  settings['auth']['api_key'] = ''

  # Data dict
  data = {}
  data['auth'] = {}
  data['report'] = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  def is_required(self, typ):
    if 'require' in self.settings['report'] and self.settings['report']['require']:
      for item in self.settings['report']['require']:
        if typ == item:
          return True
      return False
    else:
      return True
    
  def is_excluded(self, typ):
    if 'exclude' in self.settings['report'] and self.settings['report']['exclude']:
      for item in self.settings['report']['exclude']:
        if typ == item:
          return True
      return False
    else:
      return False

  def get_spectra_component (self, data, typ = 'actor'):
    # Precompile name and data keys
    pn = ''
    pd = '_'.join([typ,'data'])
    # Build the component
    comp = {}
    if data['property_type'] == 'name':
      #~ pn = '_'.join([typ,'name'])
      comp['name'] = data['property']
    elif data['property_type'] == 'uuid':
      pn = '_'.join([typ,'uuid'])
      comp['uuid'] = data['property']
    comp[pd] = {}
    for d in data:
      if d not in ['property_type', pn, 'uuid', 'property']:
        comp[d] = data[d]
    return comp

  def post_alerts(self, data):
    # Build the main statement
    statement = {}
    statement['actor'] = self.get_spectra_component(self.settings['report']['actor'], 'actor')
    statement['action'] = {'name': 'zsm_alert', 'type': 'zsm_alert'}
    statement['object'] = self.get_spectra_component(self.settings['report']['object'], 'object')
    statement['context'] = self.get_spectra_component(self.settings['report']['context'], 'context')
    statement['data'] = []
    statement['type'] = self.settings['report']['statement_type']
    for dat in data:
      if self.is_required(dat['type']) and not self.is_excluded(dat['type']):
        d = {'type': 'zsm_alert', 'data': dat}
        statement['data'].append(d)
    sj = json.dumps(statement)
    self.post_item(sj)

  def create_statement(self, data):
    if 'type' in data and 'data' in data:
      if self.is_required(data['type']) and not self.is_excluded(data['type']):
        statement = {}
        statement['actor'] = self.get_spectra_component(self.settings['report']['actor'], 'actor')
        statement['action'] = {'name': 'zsm_report', 'type': 'zsm_report'}
        statement['object'] = self.get_spectra_component(self.settings['report']['object'], 'object')
        statement['context'] = self.get_spectra_component(self.settings['report']['context'], 'context')
        statement['data'] = data
        statement['type'] = data['type']
        if 'timestamp' in data['data']:
          statement['time'] = data['data']['timestamp']
        return statement
      else:
        return False
    else:
      return False

  def post_item(self, statement_json):
    if 'api_key' in self.settings['auth'] and self.settings['auth']['api_key']:
      p = requests.post(self.settings['report']['url'], headers = self.data['auth']['head'], data = statement_json)
      self.zsm_obj.pprint('Spectra Post: ' + str(p.status_code))
    else:
      p = requests.post(self.settings['report']['url'], headers = self.data['auth']['head'], cookies = self.data['auth']['cookie'], data = statement_json)
      self.zsm_obj.pprint('Spectra Post: ' + str(p.status_code))

  def post_reports(self, data):
    # Determine whether we have single or multiple items.
    # Check the first item in the list (if it is a list) and see if it
    # is data in the right format
    statement_list = []
    if isinstance(data, list) and data and 'type' in data[0] and 'data' in data[0]:
      for item in data:
        statement_list.append(self.create_statement(item))
    else:
      statement_list.append(self.create_statement(data))
    for statement in statement_list:
      if statement:
        sj = json.dumps(statement)
        self.zsm_obj.pprint('=== Post Statement ===', 2)
        self.zsm_obj.pprint(sj, 2)
        self.zsm_obj.pprint('======================', 2)
        self.post_item(sj)
    
  def group_report_data(self, data):
    grouped_data = {}
    return_data = []
    groups = self.settings['report']['group']
    for dat in data:
      in_group = False
      for group in groups:
        self.zsm_obj.pprint(dat['type'])
        self.zsm_obj.pprint(group)
        self.zsm_obj.pprint(groups[group])
        if dat['type'] in groups[group]:
          in_group = True
          if group not in grouped_data:
            grouped_data[group] = {'type': group}
            grouped_data[group]['reporter'] = self.__class__.__name__
            grouped_data[group]['data'] = {dat['type']: dat}
          else:
            grouped_data[group]['data'][dat['type']] = dat
      if not in_group or dat['type'] in self.settings['report']['unique']:
        return_data.append(dat)
    for group in grouped_data:
      if group in grouped_data and grouped_data[group]:
        return_data.append(grouped_data[group])
    return return_data

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.zsm_obj.conf['plugins'] and self.zsm_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.zsm_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings and subsection in self.settings[section]:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          if section in self.settings:
            self.settings[section] = conf[section]

  # Authenticate with remote services, and get the 
  #
  #
  def authenticate(self):
    login = {}
    if 'api_key' not in self.settings['auth'] or not self.settings['auth']['api_key']:
      login['name'] = self.settings['auth']['name']
      login['pass'] = self.settings['auth']['pass']
      l = json.dumps(login)

      req = requests.post(self.settings['auth']['location'], data=l)

      self.data['auth']['head'] = {}
      self.data['auth']['cookie'] = {}

      if req.status_code == 200:
        r = json.loads(req.text)
        self.data['auth']['head']['X-CSRF-Token'] = r['csrf_token']
        self.data['auth']['head']['Accept'] = 'application/json'
        self.data['auth']['head']['Content-Type'] = 'application/json'
        self.data['auth']['cookie'] = req.cookies.get_dict()
    elif 'api_key' in self.settings['auth'] and self.settings['auth']['api_key']:
      self.data['auth']['head'] = {}
      self.data['auth']['head']['Accept'] = 'application/json'
      self.data['auth']['head']['Content-Type'] = 'application/json'
      self.data['auth']['head']['api-key'] = self.settings['auth']['api_key']
  
  ## Send alerts
  #
  #
  def alert(self):
    if self.settings['report']['report_alerts']:
      self.post_alerts(self.zsm_obj.data['alerts'])

  ## Generate Reports and save data
  #
  # 
  def report(self):
    if self.settings['report']['report_reports']:
      if self.settings['report']['group']:
        rdata = self.group_report_data(self.zsm_obj.data['report_data'])
        self.post_reports(rdata)
      else:
        self.post_reports(self.zsm_obj.data['report_data'])
