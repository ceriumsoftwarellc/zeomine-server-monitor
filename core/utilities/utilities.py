## This is a utilities file that contains a variety of functions that might be
# used by a variety of modules

import socket


def get_hostname():
  if socket.gethostname().find('.')>=0:
    hostname = socket.gethostname()
  else:
    hostname = socket.gethostbyaddr(socket.gethostname())[0]
  return hostname
