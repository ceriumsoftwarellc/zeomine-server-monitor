import importlib
import json
import os
import sys
import time
import yaml
from collections import OrderedDict
from os import mkdir

# ZSM Python class
class ZSM():
  ##############################################################################
  # Settings, config, and data
  ##############################################################################

  # Debug mode: Use this to bypass certain try/accept blocks
  debug_mode = False

  # Settings dict and settings defaults
  settings = {}

  settings['description'] = 'Zeomine Server Monitor'

  settings['config_path'] = ''

  settings['verbosity'] = 0
  settings['log_verbosity'] = 0
  settings['log_path'] = 'user_data/logs/zsm_default.log'

  settings['user_data'] = {}
  settings['user_data']['basepath'] = os.path.abspath('.') + '/user_data/'
  settings['user_data']['data'] = settings['user_data']['basepath'] + 'data/'
  settings['user_data']['configs'] = settings['user_data']['basepath'] + 'configs/'
  settings['user_data']['logs'] = settings['user_data']['basepath'] + 'logs/'
  settings['user_data']['plugins'] = settings['user_data']['basepath'] + 'plugins/'

  settings['load_previous_state'] = False
  settings['save_on_shutdown'] = False
  settings['state_filename'] = 'zsm_state.json'

  # Config dict
  conf = {}
  conf['plugins'] = {}

  # Data dict
  data = {}
  data['alerts'] = []
  data['metadata'] = {}
  data['plugins'] = {}
  data['report_data'] = []
  data['zsm_state'] = []

  # Plugins
  plugins = {}

  # List of remote loader items that we don't want to repeat loading
  no_load = []

  ##############################################################################
  # Init and helper functions
  ##############################################################################

  # Init function
  def __init__(self, config_path, debug = False):
    self.settings['config_path'] = config_path
    self.debug_mode = debug

  # Internal print function that also handles logging and file output
  def pprint(self, text, verbosity_threshold = 1, log_verbosity_threshold = 1):
    s = str(text)
    if log_verbosity_threshold <= int(self.settings['log_verbosity']):
      with open(self.settings['log_path'], 'a') as f:
        f.write(s + '\n')
    else:
      pass
    if verbosity_threshold <= int(self.settings['verbosity']):
      if 'output_path' in self.settings and self.settings['output_path']:
        with open(self.settings['output_path'], 'a') as f:
          f.write(s + '\n')
      else:
        sys.stdout.write(s + '\n')
        sys.stdout.flush()
    else:
      pass

  def generate_metadata(self):
    self.data['metadata']['time'] = round(time.time())
    self.data['metadata']['time_format'] = time.strftime('%Y-%m-%d %H:%M:%S%p',time.localtime())
    # Log the initialization, and also print if set to high verbosity
    self.pprint("Zeomine Server Monitor initialized on: " + self.data['metadata']['time_format'], 2, 1)

  def load_core_config(self, path):
    with open(path, 'r') as f:
      # Loop through everything in the config file, and set self.conf to file's configuration
      c = yaml.load(f)
      # Load settings
      sets = c['settings']
      for s in sets:
        self.settings[s] = sets[s]
      # Load configs
      config = c['conf']
      for item in config:
        # If we are looking at a plugin, loop through each plugin
        if item == 'plugins':
          for plug in config['plugins']:
            self.conf['plugins'][plug] = config['plugins'][plug]
        else:
          self.conf[item] = config[item]

  ##############################################################################
  # Core functions
  ##############################################################################

  ## Primary Run Function
  #
  # All settings go through the CLI parameters or the conf file. Nothing is
  # passed to this function directly.
  def run(self):
    # Generate metadata
    self.generate_metadata()

    # Load the config file
    self.load_config(self.settings['config_path'])

    # If a previous state was specified, load its data
    self.load_previous_state()

    # Get and evaluate the server data
    self.get_data()
    self.evaluate_data()

    # Authenticate with any remote services in order to send reports and alerts
    self.authenticate()

    # If we have a alert recommendation, run the report function
    self.alert()

    # Generate/send reports and save data
    # All data evaluation should be finished by this point. We are 
    # saving/exporting only, and data should not be changing at this time.
    self.report()
    
    # Run shutdown actions (clean up temporary data, etc.)
    self.shutdown()

  ## Load Configuration Files
  #
  # This will load the configuration files and specify which plugins to load
  def load_config(self, config_path):
    # Load each config item in the list. Later ones will overwrite earlier ones.
    # Since the log file settings are in flux, we invalidate logging by setting
    # to a high number.
    for conf in config_path:
      # Absolute UNIX path
      if conf[0] == '/':
        path = os.path.abspath(conf)
      # Windows path: C: or D:, etc.
      elif conf[1] == ':':
        path = os.path.abspath(conf)
      # If not absolute, assume relative to the user data path
      else:
        path = self.settings['user_data']['configs'] + conf
      self.pprint('Importing config ' + path, 1, 999)
      if self.debug_mode:
        self.load_core_config(path)
      else:
        try:
          self.load_core_config(path)
        except:
          self.pprint("Failed to import " + path, 0, 999)
      # Debug: print settings and configs
      self.pprint('Settings: ', 2, 2)
      self.pprint(self.settings, 2, 2)
      self.pprint('Config: ', 2, 2)
      for a in self.conf:
        for b in self.conf[a]:
          self.pprint(a + ' - ' + b, 2, 2)
          self.pprint(self.conf[a][b], 2, 2)
      # Load the plugins
      for plug in self.conf['plugins']:
        self.pprint("Loading plugin " + plug, 1, 1)
        if self.debug_mode:
          plugin_bases = {"core": "core.plugins", "sample": "samples", "custom": "user_data.plugins"}
          p = self.conf['plugins'][plug]
          c = self.conf['plugins'][plug]['class']
          m = importlib.import_module(plugin_bases[p['type']] + '.' + p['module'])
          plugin = getattr(m,c)
          # Check to see if we already added this plugin
          chk = []
          for plu in self.plugins:
            chk.append(str(plu.__class__))
          if str(c) not in chk:
            # We can add this plugin. Also, set the zsm_obj
            self.plugins[c] = plugin()
            self.plugins[c].zsm_obj = self
        else:
          try:
            plugin_bases = {"core": "core.plugins", "sample": "samples", "custom": "user_data.plugins"}
            p = self.conf['plugins'][plug]
            c = self.conf['plugins'][plug]['class']
            m = importlib.import_module(plugin_bases[p['type']] + '.' + p['module'])
            plugin = getattr(m,c)
            # Check to see if we already added this plugin
            chk = []
            for plu in self.plugins:
              chk.append(str(plu.__class__))
            if str(c) not in chk:
              # We can add this plugin. Also, set the zsm_obj
              self.plugins[c] = plugin()
              self.plugins[c].zsm_obj = self
          except:
            self.pprint("Plugin " + plug + " could not be imported, most likely due to missing configuration items", 0)
      
      # Finally, lets call the load_config method for each plugin, so they can carry out their startup actions.
      for plugin in self.plugins:
        if callable(getattr(self.plugins[plugin], 'load_config', False)):# \
          remote_load = self.plugins[plugin].load_config()
          if remote_load:
            break

  ## Load Previous State for running comparisons
  #
  # Load ZSM's state data first, as some modules may need its info.
  def load_previous_state(self):
    if self.settings['load_previous_state']:
    # Use a try block as the file may not exist the first time a config is used
      try:
        with open(os.path.abspath(self.settings['user_data']['data'] + self.settings['state_filename']), 'r') as f:
          self.data['zsm_state'] = json.load(f)
      except:
        path = self.settings['user_data']['data'] + self.settings['state_filename']
        self.pprint('Attempted to load ' + path + ', but the file does not exist or failed to load.', 0, 0)
    # Always let the plugins self manage whether to save - some of them 
    # depend on a save state to function correctly.
    for plugin in self.plugins:
      if callable(getattr(self.plugins[plugin], 'load_previous_state', False)):
        self.plugins[plugin].load_previous_state()

  ## Collect Data
  #
  # 
  def get_data(self):
    for plugin in self.plugins:
      if callable(getattr(self.plugins[plugin], 'get_data', False)):
        self.plugins[plugin].get_data()

  ## Evaluate Data
  #
  # 
  def evaluate_data(self):
    for plugin in self.plugins:
      if callable(getattr(self.plugins[plugin], 'evaluate_data', False)):
        self.plugins[plugin].evaluate_data()

  ## Authenticate with Remote services for alerting/reporting
  #
  # 
  def authenticate(self):
    for plugin in self.plugins:
      if callable(getattr(self.plugins[plugin], 'authenticate', False)):
        self.plugins[plugin].authenticate()

  ## Send Alerts
  #
  # This is meant to fire only if we have alert data
  def alert(self):
    if self.data['alerts']:
      for plugin in self.plugins:
        if callable(getattr(self.plugins[plugin], 'alert', False)):
          self.plugins[plugin].alert()

  ## Generate Reports
  #
  # 
  def report(self):
    for plugin in self.plugins:
      if callable(getattr(self.plugins[plugin], 'report', False)):
        self.plugins[plugin].report()

  ## Final actions for ZSM shutdown
  #
  # 
  def shutdown(self):
    # Let plugins run first, as ZSM will perform the final save state if set
    for plugin in self.plugins:
      if callable(getattr(self.plugins[plugin], 'shutdown', False)):
        self.plugins[plugin].shutdown()

    # Save data if requested
    if self.settings['save_on_shutdown']:
      # Set up data
      state = {}
      state['alerts'] = self.data['alerts']
      state['report_data'] = self.data['report_data']
      state['metadata'] = self.data['metadata']
      state['metadata']['description'] = self.settings['description']
      state['plugins'] = self.data['plugins']
      state['settings'] = self.settings
      state['conf'] = self.conf
      self.data['zsm_state'].append(state)
      with open(os.path.abspath(self.settings['user_data']['data'] + self.settings['state_filename']), 'w') as f:
        write_data = json.dumps(self.data['zsm_state'])
        f.write(write_data)

    # Clear one-time data
    self.data['alerts'] = []
