import requests
import json

# Endpoints
login_endpoint = 'http://example.com/user/login/'
post_endpoint = 'http://example.com/path/to/endpoint/'

# Authenticate
login = {}
login['name'] = ''
login['pass'] = ''
l = json.dumps(login)

req = requests.post(login_endpoint, data=l)

head = {}
cook = {}

if req.status_code == 200:
  r = json.loads(req.text)
  head['X-CSRF-Token'] = r['csrf_token']
  head['Accept'] = 'application/json'
  head['Content-Type'] = 'application/json'
  cook = req.cookies.get_dict()


  add = {}
  aj = json.dumps(add)
  p = requests.post(post_endpoint, headers = head, cookies = cook, data = aj)
else:
  print('Failed to authenticate.')
