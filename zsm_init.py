import argparse
import time
from core.zsm import ZSM

# Handle arguments
parser = argparse.ArgumentParser(description="Inititates Zeomine Server Monitor")
parser.add_argument('--conf', help='Path to config file in YAML format, relative to user_data/configs')
parser.add_argument('--repeat_count', type=int, help='Repeat a specified number of times. Defaults to 0 (run once)')
parser.add_argument('--repeat_time', type=float, help='Time to wait until the next run. If the first run goes over this time, the next run will wait until it is finished.')
parser.add_argument('--debug', type=int, help='Bypasses certain try/accept blocks to enable debugging. Can be 1 or 0')
args = parser.parse_args()

# Get configuration list
config_path = args.conf.split(',')
repeat_count = args.repeat_count if args.repeat_count else 0
repeat_time = args.repeat_time if args.repeat_time else 0
debug = True if args.debug else False

repeats = 0
if config_path and isinstance(config_path, list):
  next_run_time = 0
  while repeats <= repeat_count:
    if time.time() > next_run_time:
      next_run_time = time.time() + repeat_time
      zsm = ZSM(config_path, debug)
      zsm.run()
      repeats += 1
    if repeats < repeat_count:
      time.sleep(0.5)
