# Zeomine Server Monitor #

## CLI Commands ##

ZSM is run on a command line, and under normal conditions you pass a single 
parameter in the form of a path to the config file, relative to the ZSM 
user_data/configs directory. Here's an example for an Ubuntu server:

python3 /path/to/zeomine.py --conf=example.yml

In Windows, you might use "py" instead of "python3"

## Major steps in operation ##

Zeomine Server Monitor (ZSM) is initiated either manually or by cron. Each 
instance will go through the same basic steps:

* Initiate monitor and generate metadata for the run instance
* Import data, such as a save state that will be compared against a new state
* Collect data
* Evaluate data
* Send alerts if necessary
* Create reports and save data

## Overall structure of data ##

The data is structured as a keyed array in JSON format.

Data is stored per plugin, and within each plugin you have an "import",
"current", and "calculated" dataset. Metadata for the entire instance is 
stored in a "metadata" key at the plugin level.

## Folder Structure ##

* core: This folder contains the core logic for executing Zeomine Server Monitor
* plugins: These are optional plugins you may call, given your use case
* samples: These are sample configs and plugins you can use as examples for custom code
* user_data: This is where you place your custom code. 

### User folder ###

In the user_data folder, the following directories are available:

* configs: These are the configuration files that you use to run ZSM.
* plugins: Custom plugins go here
* data: Data will be placed here
