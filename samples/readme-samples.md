### Zeomine Server Monitor Samples ###

## Sample Config ##

This config will perform a basic monitoring of your server, using the standard
plugins. You may copy it to your configs folder to try.

## Sample Plugin ##

The sample plugin will work if copied to the custom folder and allowed to run. 
It will provide you with examples of how ZSM works.
