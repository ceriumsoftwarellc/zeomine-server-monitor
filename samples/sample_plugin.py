from core.plugins.zsm_plugin import ZSMPlugin

## Sample Plugin
#
class SamplePlugin(ZSMPlugin):
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  zsm_obj = None
  
  # Settings dict
  settings = {}

  # Local data dict
  data = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  ## Create an alert recommendation
  #
  # Standard thresholds are notice, warning, critical, and highly_critical.
  # You may create a custom severity to create extra user-specific cases.
  def create_alert(self, data, alert_type, threshold_value, severity = 'notice'):
    recommendation = {}
    recommendation['severity'] = severity
    recommendation['type'] = alert_type
    recommendation['threshold_value'] = threshold_value
    recommendation['data'] = data

    recommendation['reporter'] = self.__class__.__name__ # Usually the class name

    self.zsm_obj.data['alerts'].append(recommendation)

  # Common alert function: get the highest matching threshold
  def get_highest_threshold(self, val, threshold_dict):
    ret = False
    for t in threshold_dict:
      if val > threshold_dict[t]:
        ret = t
      else:
        pass
    return ret

  # Common alert function: get the lowest matching threshold
  def get_lowest_threshold(self, val, threshold_dict):
    ret = False
    for t in threshold_dict:
      if val < threshold_dict[t]:
        ret = t
      else:
        pass
    return ret

  ##############################################################################
  # ZSM plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    self.zsm_obj.pprint("Successfully loaded SamplePlugin.")
